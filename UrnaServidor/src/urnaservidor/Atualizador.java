/*
 * Nomes(NºUSP) :
 * 
 * Fabrício Pereira de Freitas (8910372)
 * Pedro Carrascosa C. Dias (7173480)
 * Ricardo Chagas (8957242)
 */
package urnaservidor;

import java.util.ArrayList;
import urnacomum.Candidato;

/**
 * Classe responsável por manter os dados do servidor consistentes e imprimir estado atual da votação
 * 
 */
public class Atualizador implements Runnable {
    private final ArrayList<Candidato> candidatos;
    private final ArrayList<Urna> urnas;
    private int brancos;
    private int nulos;

    public Atualizador(ArrayList<Candidato> candidatos, ArrayList<Urna> urnas) {
        this.candidatos = new ArrayList(candidatos);
        this.urnas = urnas;
        this.brancos = 0;
        this.nulos = 0;
    }

    public int getBrancos() {
        return brancos;
    }

    public int getNulos() {
        return nulos;
    }

    public void setBrancos(int brancos) {
        this.brancos = brancos;
    }

    public void setNulos(int nulos) {
        this.nulos = nulos;
    }
    
    /**
     * Método que pega a lista recebida e soma os resultados desta na total
     * @param novaLista - Lista com votos a serem somados com o total
     */
    public synchronized void mergeListaCandidatos(ArrayList<Candidato> novaLista) {
        for(Candidato candidato: this.candidatos) {
            candidato.setNum_votos(candidato.getNum_votos() + novaLista.get(this.candidatos.indexOf(candidato)).getNum_votos()); //Atualiza os votos do candidato
        }
    }
    
    /**
     * Remove a conexão de uma urna que se desconectou
     * @param urna - urna a ser desconectada
     */
    public synchronized void removeUrna(Urna urna) {
        urnas.remove(urna);
    }
    
    @Override
    public void run() {
        while(true) {
            //Realiza a impressão dos votos------------------------------------------------------------------
            Candidato primeiro;
            Candidato segundo;
            Candidato terceiro;
            
            //Pega os três candidatos mais votados
            primeiro = candidatos.get(0);
            segundo = candidatos.get(1);
            terceiro = candidatos.get(2);
            for(Candidato candidato: candidatos) {
                if(candidato.getNum_votos() > primeiro.getNum_votos()) {
                    terceiro = segundo;
                    segundo = primeiro;
                    primeiro = candidato;
                } else if(candidato.getNum_votos() > segundo.getNum_votos()) {
                    terceiro = segundo;
                    segundo = candidato;
                } else if(candidato.getNum_votos() > terceiro.getNum_votos()) {
                    terceiro = candidato;
                }
            }
            System.out.print("\033[H\033[2J");
            System.out.flush();
            System.out.println("Candidato mais votado ate o momento:");
            System.out.println(primeiro);
            System.out.print("\n");
            System.out.println("Segundo candidato mais votado ate o momento:");
            System.out.println(segundo);
            System.out.print("\n");
            System.out.println("Terceiro candidato mais votado ate o momento:");
            System.out.println(terceiro);
            System.out.print("\n");
            System.out.println("Votos Brancos: " + this.brancos + "\tVotos Nulos: " + this.nulos);
            
            try {
                Thread.sleep(1000);
            } catch (InterruptedException ex) {
            }
                //-----------------------------------------------------------------------------------------------
        }
    }
    
}
