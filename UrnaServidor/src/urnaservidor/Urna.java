/*
 * Nomes(NºUSP) :
 * 
 * Fabrício Pereira de Freitas (8910372)
 * Pedro Carrascosa C. Dias (7173480)
 * Ricardo Chagas (8957242)
 */
package urnaservidor;

import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.util.ArrayList;
import urnacomum.Candidato;
import urnacomum.Mensagem;

/**
 * Esta classe é resposável pelo gerenciamento de cada conexão
 * 
 */
public class Urna implements Runnable {
    private final ObjectOutputStream output;
    private final ObjectInputStream input;
    private final ArrayList<Candidato> listaCandidatos;
    private final Atualizador atualizador;

    public Urna(ObjectOutputStream output, ObjectInputStream input, ArrayList<Candidato> listaCandidatos, Atualizador atualizador) {
        this.output = output;
        this.input = input;
        this.listaCandidatos = listaCandidatos;
        this.atualizador = atualizador;
    }
    
    @Override
    public void run() {
        System.out.println("Nova urna iniciada");
        
        boolean run = true;
        while(run) {
            Mensagem msg;
            
            try {
                msg = (Mensagem) this.input.readObject(); //Tenta ler a mensagem
                System.out.println("Mensagem recebida");
            } catch (Exception e) {
                System.out.println("Nao foi possivel ler a mensagem");
                break;
            }
            
            //Verifica qual é o tipo da mensagem
            switch(msg.getOpcode()) {
                case 999:
                    try {
                        this.output.writeObject(listaCandidatos);
                        System.out.println("Mensagem enviada");
                    } catch (Exception e) {
                        System.out.println("Erro ao enviar mensagem");
                    }
                    break;
                    
                case 888:
                    this.atualizador.mergeListaCandidatos(msg.getDados());
                    this.atualizador.setBrancos(this.atualizador.getBrancos() + msg.getBrancos());
                    this.atualizador.setNulos(this.atualizador.getNulos() + msg.getNulos());
                    this.atualizador.removeUrna(this);
                    run = false;
                    break;
            }
        }
    }   
}
