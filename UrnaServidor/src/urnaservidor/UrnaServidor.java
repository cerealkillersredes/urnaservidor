/*
 * Nomes(NºUSP) :
 * 
 * Fabrício Pereira de Freitas (8910372)
 * Pedro Carrascosa C. Dias (7173480)
 * Ricardo Chagas (8957242)
 */
package urnaservidor;

import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.net.ServerSocket;
import java.net.Socket;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Scanner;
import urnacomum.Candidato;

/**
 * Classe responsável por ouvir a porta e estabelacer as conexões
 * 
 */
public class UrnaServidor {

    /**
     * @param args
     * @throws IOException
     * @throws ClassNotFoundException 
     */
    public static void main(String[] args) throws IOException, ClassNotFoundException {
        Atualizador atualizador;
        ArrayList<Candidato> candidatos;
        ArrayList<Urna> urnas;
        ServerSocket server;
        Socket socket;
        Scanner scanner;
        int porta;
        
        //Pega a porta a ser usada pelo grupo
        scanner = new Scanner(System.in);
        System.out.print("Digite a porta que deseja escutar: ");
        porta = scanner.nextInt();
        
        //Inicializa vetor de candidatos--------------------------------------------------------------------
        candidatos = new ArrayList();
        populate(candidatos);
        //---------------------------------------------------------------------------------------------------    

        urnas = new ArrayList();
        atualizador = new Atualizador(candidatos, urnas); //Inicializa o atualizador
        Thread att = new Thread(atualizador); //Inicializa a thread do atualizador
        att.start();
        server = new ServerSocket(porta); //porta fornecida
        //Inicia a espera por novas conexões ao servidor-----------------------------------------------------
        while(true) {
            try {
                socket = server.accept(); //Faz nova conexão
            } catch (Exception e) {   //Houve falha na conexão
                System.out.println("Nao foi possivel receber a conexao");
                continue;
            }
            
            System.out.println("Nova urna conectada");
            
            try {
                ObjectInputStream input = new ObjectInputStream(socket.getInputStream());
                ObjectOutputStream output = new ObjectOutputStream(socket.getOutputStream());
                Urna novaUrna = new Urna(output, input, candidatos, atualizador);
                
                urnas.add(novaUrna);
                Thread urna = new Thread(novaUrna);
                urna.start();
                System.out.println("Nova conexao estabelecida");
            } catch (Exception e) {
                System.out.println("Nao foi possivel estabelecer conexao");
            }
        }
        //---------------------------------------------------------------------------------------------------
    }
    
    //Lista de candidatos------------------------------------------------------------------------------------
    private static void populate(ArrayList<Candidato> listaCandidatos) {
        Candidato[] lista = {
                                new Candidato(1331, "Jose Alberto", "P1"),
                                new Candidato(1332, "Joao Ribeiro", "P2"),
                                new Candidato(1333, "Maria Leite", "P3"),
                                new Candidato(1334, "Monica Martins", "P4"),
                                new Candidato(1335, "Geraldo Picole de Chuchu", "P5"),
                                new Candidato(1336, "Reinaldo Dias", "P7"),
                                new Candidato(1337, "Sauron", "P8"),
                                new Candidato(1338, "Gandalf", "P9"),
                                new Candidato(1339, "Frodo Baggins", "P10"),
                                new Candidato(1310, "Will", "P11")
                                };
        
        listaCandidatos.addAll(Arrays.asList(lista));
    }
    //-------------------------------------------------------------------------------------------------------
}
